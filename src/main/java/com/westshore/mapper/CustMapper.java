package com.westshore.mapper;

import com.westshore.domain.Company;
import com.westshore.domain.Cust;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Component;

import java.util.List;

public interface CustMapper {

    @Select("SELECT * FROM dbo.Cust WHERE CustKey = #{id}")
    Cust findCust(@Param("id") String id);


    @Select("SELECT * FROM dbo.Cust")
    List<Cust> findAllCust();


    @Select("SELECT * FROM dbo.Company")
    List<Company> findAllCompany();

}
