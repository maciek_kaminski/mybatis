package com.westshore;

import com.westshore.domain.Company;
import com.westshore.domain.Cust;
import com.westshore.mapper.CustMapper;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.List;

@SpringBootApplication
@MapperScan("com.westshore.mapper")
public class MyBatisApplication implements CommandLineRunner {

	@Autowired
	private CustMapper custMapper;

	public static void main(String[] args) {
		SpringApplication.run(MyBatisApplication.class, args);
	}

	@Override
	public void run(String... strings) throws Exception {
		List<Cust> cust = this.custMapper.findAllCust();
		List<Company> allCompany = this.custMapper.findAllCompany();

		System.out.println(cust);

	}
}
