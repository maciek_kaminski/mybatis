package com.westshore.domain;

import java.io.Serializable;


public class Cust implements Serializable {

    private String CustKey;

    private String CustName;

    private String ShortName;

    private String CompanyKey;

    private Integer PileColor;

    public String getCustKey() {
        return CustKey;
    }

    public void setCustKey(String custKey) {
        CustKey = custKey;
    }

    public String getCustName() {
        return CustName;
    }

    public void setCustName(String custName) {
        CustName = custName;
    }

    public String getShortName() {
        return ShortName;
    }

    public void setShortName(String shortName) {
        ShortName = shortName;
    }

    public String getCompanyKey() {
        return CompanyKey;
    }

    public void setCompanyKey(String companyKey) {
        CompanyKey = companyKey;
    }

    public Integer getPileColor() {
        return PileColor;
    }

    public void setPileColor(Integer pileColor) {
        PileColor = pileColor;
    }
}