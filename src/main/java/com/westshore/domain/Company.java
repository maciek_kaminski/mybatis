package com.westshore.domain;

import java.io.Serializable;

public class Company implements Serializable {

    private String CompanyKey;

    private String CompanyName;

    private String isActive;

    public String getCompanyKey() {
        return CompanyKey;
    }

    public void setCompanyKey(String companyKey) {
        CompanyKey = companyKey;
    }

    public String getCompanyName() {
        return CompanyName;
    }

    public void setCompanyName(String companyName) {
        CompanyName = companyName;
    }

    public String getActive() {
        return isActive;
    }

    public void setActive(String active) {
        isActive = active;
    }
}
